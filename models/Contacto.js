const mongoose = require("mongoose")
const moment = require("moment-timezone")

const ContactoSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
    trim: true,
  },
  apellidos: {
    type: String,
    required: true,
    trim: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
  },
  mensaje: {
    type: String,
    required: true,
  },
  empresa: {
    type: String,
  },
  fecha: {
    type: Date,
    default: moment.tz(Date.now(), "Europe/Madrid"),
  },
  respondido: {
    type: Boolean,
    default: false,
  },
})

module.exports = mongoose.model("Contacto", ContactoSchema)
