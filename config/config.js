const config = {
  errors: {
    SESION_EXPIRADA: "_1",
    TOKEN_INVALIDO: "_2",
    INCORRECT_PASSWORD: "_3",
  },
}

module.exports = config
