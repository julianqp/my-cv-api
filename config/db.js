const mongoose = require("mongoose")
require("dotenv").config({ path: "variables.env" })

const connectDB = async () => {
  const URL = process.env.URL_MONGO
  const DB = process.env.DB
  const USERDB = process.env.USERDB
  const PASS = process.env.PASS

  const urlMongo = `mongodb+srv://${USERDB}:${PASS}@${URL}/${DB}?retryWrites=true&w=majority`

  try {
    await mongoose.connect(urlMongo, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    console.log("DB Conectada")
  } catch (error) {
    console.log("HUBO UN ERROR")
    console.log(error.message)

    process.exit(1) // Detiene el proceso
  }
}

module.exports = connectDB
