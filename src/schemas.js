const { gql } = require("apollo-server")

const typeDefs = gql`
  type Contacto {
    id: ID
    nombre: String
    apellidos: String
    email: String
    empresa: String
    mensaje: String
    fecha: String
    respondido: Boolean
  }
  type ContactoOut {
    OK: Boolean
    contactos: [Contacto]
    error: Error
  }

  type Token {
    token: String
    error: Error
  }

  type Error {
    msn: String
    code: String
  }

  type Default {
    OK: Boolean
    error: Error
  }

  input ContactoInput {
    nombre: String
    apellidos: String
    email: String
    mensaje: String
    empresa: String
  }

  input MensajeInput {
    id: ID
    respuesta: String
    token: String
  }

  type Query {
    getContactos(token: String!): ContactoOut
    isLogin(token: String!): Default
  }

  type Mutation {
    saveContacto(input: ContactoInput!): Boolean
    login(pass: String!): Token
    contestar(input: MensajeInput!): Default
  }
`

module.exports = typeDefs
