// Importamos los modelos
const Contacto = require("../models/Contacto")
const senEmail = require("../util/senEmail")
const responderEmail = require("../util/responderEmail")
const createToken = require("../util/token")
const { getToken } = require("../util/token")
const config = require("../config/config")
// importamos las librerias necesarias
require("dotenv").config({ path: "variables.env" })

const resolvers = {
  Query: {
    getContactos: async (_, { token }) => {
      try {
        const { OK } = getToken(token)
        if (!OK) {
          return {
            OK: false,
            error: {
              msn: "Sin autorización",
              code: 10,
            },
          }
        }
        const contactos = await Contacto.find({})

        return { OK: true, contactos }
      } catch (error) {
        // En caso de error devolvemos el error
        return {
          OK: false,
          error: {
            msn: error.message,
          },
        }
      }
    },
    isLogin: async (_, { token }) => {
      return getToken(token)
    },
  },
  Mutation: {
    saveContacto: async (_, { input }) => {
      try {
        const formulario = new Contacto(input)
        formulario.save()
        try {
          await senEmail(input)
        } catch (error) {
          console.log(error)
          return false
        }
        return true
      } catch (error) {
        throw new Error(error.message)
      }
    },
    login: async (_, { pass }) => {
      if (pass === process.env.LOGINPASS) {
        const token = createToken()
        return { token }
      }
      return {
        error: {
          msn: "La contraseña es incorrecta.",
          code: config.errors.INCORRECT_PASSWORD,
        },
      }
    },
    contestar: async (_, { input }) => {
      const { id, respuesta, token } = input
      const { OK } = getToken(token)
      if (!OK) {
        return {
          OK: false,
          error: {
            msn: "Sin autorización",
            code: 10,
          },
        }
      }
      const existeMensaje = await Contacto.findById(id)

      if (!existeMensaje) {
        return {
          OK: false,
          error: {
            msn: "Mensaje no encontrado.",
            code: 9,
          },
        }
      }
      const datos = {
        mensaje: existeMensaje.mensaje,
        respuesta,
        email: existeMensaje.email,
      }
      try {
        await responderEmail(datos)
        existeMensaje.respondido = true
        await Contacto.findByIdAndUpdate(id, { respondido: true })
        return { OK: true }
      } catch (error) {
        return {
          OK: false,
          error: {
            msn: error.message,
            code: 9,
          },
        }
      }
    },
  },
}

module.exports = resolvers
