module.exports = (nombre, apellidos, email, mensaje, empresa) => {
    return `<!DOCTYPE html>

<head>
  <style>
    .text-uppercase {
      text-transform: uppercase;
    }

    .text-center {
      text-align: center;
    }

    .m-4 {
      margin: 4px;
    }

    .m-5 {
      margin: 5px;
    }

    .d-flex {
      display: flex;
    }

    .mr-4 {
      margin-right: 4px;
    }

    .btn {
      display: inline-block;
      font-weight: 400;
      color: #212529;
      text-align: center;
      vertical-align: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-color: transparent;
      border: 1px solid transparent;
      padding: 0.375rem 0.75rem;
      font-size: 1rem;
      line-height: 1.5;
      border-radius: 0.25rem;
      transition: color 0.15s ease-in-out,
        background-color 0.15s ease-in-out,
        border-color 0.15s ease-in-out,
        box-shadow 0.15s ease-in-out;
    }
   .text-decoration-none {
  text-decoration: none !important;
  color: black;
}
 .text-decoration-none:hover {
  color: white;
}
    .btn:hover {
       color: white ;
      text-decoration: none;
    }

    .btn-outline-dark {
      color: #343a40;
      border-color: #343a40;
    }

    .btn-outline-dark:hover {
      color: white;
      background-color: #343a40;
      border-color: #343a40;
    }

    .btn-lg,
    .btn-group-lg>.btn {
      padding: 0.5rem 1rem;
      font-size: 1.25rem;
      line-height: 1.5;
      border-radius: 0.3rem;
    }
  </style>
</head>

<body>
  <h1 class="text-uppercase text-center m-4">Nuevo mensaje de contacto</h1>
  <div class="m-5">
    <p class="">Alguién ha contactado contigo a través del cv web.</p>
    <h2>Datos de contacto</h2>
    <div class="d-flex flex-row bd-highlight">
      <p class="mr-4"><b>Nombre</b></p>
      <p>${nombre}</p>
    </div>
    <div class="d-flex flex-row bd-highlight">
      <p class="mr-4"><b>Apellidos</b></p>
      <p>${apellidos}</p>
    </div>
     <div class="d-flex flex-row bd-highlight">
      <p class="mr-4"><b>Empresa</b></p>
      <p>${empresa}</p>
    </div>
    <div class="d-flex flex-row bd-highlight">
      <p class="mr-4"><b>Email</b></p>
      <p>${email}</p>
    </div>
    <div class="d-flex flex-row bd-highlight">
      <p class="mr-4"><b>Mensaje</b></p>
      <p>${mensaje}</p>
    </div>
  </div>
  <div class="text-center">
      <a class='text-decoration-none' href='mailto:${email}?subject=Respuesta cv Julián Querol&body=Mensaje: "${mensaje}"'>
        <button type="button" class="btn btn-outline-dark btn-lg">Responder </button></a>
  </div>
  <br />
  <br />
  <div>
    <p class="text-center font-weight-light">
      Mensaje automático enviado a traves de cv web.
    </p>
  </div>
</body>
</html>`
}