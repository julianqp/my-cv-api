module.exports = (mensaje, respuesta) => {
  return `<!DOCTYPE html>
  <head>
    <style>
        *{
            font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
            margin: 5px;
        }
        p{
            margin: 0;
            padding: 0;
            margin-top: 1px;
            margin-bottom: 1px;
        }
      .caja {
          background-color: #e0f7fa;
          padding: 10px;
          margin: 10px;
          border-radius: 25px;
      }
      .caja-respuesta {
          background-color: #eceff1;
          padding: 10px;
          margin: 10px;
          margin-top: 20;
          margin-bottom: 20px;
          border-radius: 25px;
      }
      .comillas {
          font-size: 36px;
      }
    </style>
  </head>
  
  <body>
    <h2 class="">Respuesta CV Julián Querol</h1>
    <div class="">
        
      <p class="">Buenas tardes,</p>
      <p class="">Soy Julián Querol Polo, has contactado conmigo a traves de <a href="https://julian.querol.site" target="_blank">https://julian.querol.site</a>, ecribiendo el siguiente mensaje:</p>
      <br/>
      <div class="caja">
          <span class="comillas">"</span>
          ${mensaje}
          <span class="comillas">"</span>
      </div>
      <h5>Respuesta:</h5>
      <br/>
      ${respuesta}
    </div>
  </body>
  </html>`
}
