const nodemailer = require("nodemailer")
const respuestaTemplate = require("../templates/responder")
const config = require("../config/config")
require("dotenv").config({ path: "variables.env" })

const responderEmail = async ({ mensaje, respuesta, email }) => {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount()

  let transporter = nodemailer.createTransport({
    service: "gmail",
    debug: true,
    secure: true,
    auth: {
      user: process.env.EMAIL, // generated ethereal user
      pass: process.env.EMAILPASS, // generated ethereal password
    },
  })

  const apliteado = respuesta.split("\n")
  let texto = apliteado.map(x => {
    if (x.length > 0) {
      return `<p>${x}</p>`
    }
    return `<br />`
  })
  texto = texto.join("")

  let info
  try {
    info = await transporter.sendMail({
      from: `"Julián Querol CV" <${process.env.EMAIL}>`, // sender address
      to: `${email}`, // list of receivers
      subject: "Respues CV contacto Julian", // Subject line
      html: respuestaTemplate(mensaje, texto), // html body
    })
  } catch (e) {
    console.log(e)
  }

  console.log("Message sent: %s", info.messageId)
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info))
}

module.exports = responderEmail
