const nodemailer = require("nodemailer")
const contactoTemplate = require("../templates/contacto")
const config = require("../config/config")
require("dotenv").config({ path: "variables.env" })

const senEmail = async ({ nombre, apellidos, email, mensaje, empresa }) => {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount()

  let transporter = nodemailer.createTransport({
    service: "gmail",
    debug: true,
    secure: true,
    auth: {
      user: process.env.EMAIL, // generated ethereal user
      pass: process.env.EMAILPASS, // generated ethereal password
    },
  })

  // send mail with defined transport object

  let info
  try {
    info = await transporter.sendMail({
      from: '"CV Web Julián" <web@julianquerol.site>', // sender address
      to: `${process.env.EMAIL}`, // list of receivers
      subject: "Mensaje CV contacto", // Subject line
      html: contactoTemplate(nombre, apellidos, email, mensaje, empresa), // html body
    })
  } catch (e) {
    console.log(e)
  }

  console.log("Message sent: %s", info.messageId)

  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info))
}

module.exports = senEmail
