const jwt = require("jsonwebtoken")

createToken = () => {
  const token = jwt.sign({ id: "pc" }, process.env.JWTSECRET, {
    expiresIn: "24 h",
  })
  return token
}

const getToken = token => {
  try {
    jwt.verify(token, process.env.JWTSECRET)
    return { OK: true }
  } catch (e) {
    console.log(e.message)
    // invalid algorithm
    // jwt expired
    // jwt malformed
    return {
      OK: false,
      error: e.message,
    }
  }
}

module.exports = createToken
module.exports.getToken = getToken
