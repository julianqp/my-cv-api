const { ApolloServer } = require("apollo-server")
const typeDefs = require("./src/schemas")
const resolvers = require("./src/resolvers")
const connectDB = require("./config/db")

require("dotenv").config({ path: "variables.env" })

// Conectar a la base de datos
connectDB()

// Servidor
const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
})

// Arrancar el servidor
server.listen({ port: process.env.PORT || 4000 }).then(({ url }) => {
  console.log(`Servidor listo en la URL ${url}`)
})
